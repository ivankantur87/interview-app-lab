import React from 'react';
import TodoList from './Todo/TodoList';
import Context from './context';
import AddTodo from './Todo/AddTodo';
import moment from "moment";
// import TodoItem from './Todo/TodoItem';
// import PropTypes from 'prop-types'

function App() {
  const [todos, setTodos] = React.useState([
    {id:1, completed: false, corrected: false, title: 'Купить хлеб', date: moment().format("DD-MM-YYYY hh:mm:ss")},
    {id:2, completed: false, corrected: false, title: 'Полить цветы', date: moment().format("DD-MM-YYYY hh:mm:ss")},
    {id:3, completed: false, corrected: false, title: 'Покормить собаку', date: moment().format("DD-MM-YYYY hh:mm:ss")}
  ])
  // let storage = ''

  function toggleTodo(id){
    setTodos( 
      todos.map(todo=> {
          if (todo.id === id) {
            todo.completed = !todo.completed;
          }
        return todo;
      })
    )
  }


  function removeTodo(id) {
    setTodos(todos.filter(todo=> todo.id !== id))
  }

  function addTodo(title) {
    if (todos.filter(todo => todo.title === title).length) return;

    setTodos(todos.concat([{
      className: 'titleClass',
      title: title,
      id: Date.now(),
      completed: false,
      corrected: false,
      date: moment().format("DD-MM-YYYY hh:mm:ss")
    }]))
  }

  function correctedTitle(event) {
    event.preventDefault()
    console.log('new title', event.target.value)
  //   setTodos(todos.map(todo=> {
  //       if (todo.id ===id){
  //         todos.splise(todo.id, 0, [{
  //           className: 'titleClass',
  //           title: {storage},
  //           id: Date.now(),
  //           completed: false,
  //           corrected: false,
  //           date: moment().format("DD-MM-YYYY hh:mm:ss")
  //         }])
          
  //       }
  //       return (
  //         todo,
  //         console.log('new title'))
  //   }))
  }

  function DClickTodo(id) {
    setTodos(
      todos.map(todo=> {
        todo.corrected = false;
        if (todo.id === id){
          todo.corrected = true;
        }
        return todo
      })
    )
  }

  

  return (
    <Context.Provider value={{removeTodo: removeTodo}}>
      <div className="wrapper">
        <h1>React tutorial</h1>
        <AddTodo onCreate={addTodo}/>
        {todos.length ? (
          <TodoList todos={todos} onToggle={toggleTodo} onDoubleC={DClickTodo} correctedTitle={correctedTitle}/>
        ) : (
          <h1> No todos!!!</h1>
        )}
      </div>
    </Context.Provider>

  );
}

export default App