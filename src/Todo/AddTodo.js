import React, { useState } from "react";
import PropTypes from "prop-types"

function AddTodo({onCreate}){

    function submitHandler(event) {
        event.preventDefault()
        let valueTrimmed = value.trim()

        if (!valueTrimmed) return
        onCreate(valueTrimmed)
        setValue('')
        
    }

    const [value, setValue] = useState('')
    return(
        <form style={{marginBottom: '1rem'}} onSubmit={submitHandler}>
            <input value={value} placeholder={'Ваша новая задача'} onChange={event => setValue(event.target.value)}/>
            <button type='submit'> Добавить задачу</button>
        </form>
    )
}

AddTodo.propTypes ={
    onCreate : PropTypes.func.isRequired
}

export default AddTodo