import React, {useContext} from "react";
import PropTypes from "prop-types";
import Context from "../context";
import '../index.css'

const styles = {
    li: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: '.5rem 1rem',
        border: '1px solid #ccc',
        borderRadius: '4px',
        marginBottom: '.5rem',
        textAlign: 'right'
    },
    input: {
        marginRight: '1rem'
    },
    textId: {
        color: 'green'
    }
}

function TodoItem({ todo, index, onChange, onDoubleClick }) {
    const {removeTodo} = useContext(Context);
    const classes = [];
    const titleClass =['titleClass'];

    if (todo.completed) {
        classes.push('done')
    }

    return(
        <li style={styles.li}>
            <span className={classes.join(' ')}>
                <input  
                    type="checkbox" 
                    checked={todo.completed}
                    style={styles.input} 
                    onChange={()=>onChange(todo.id)}
                />
                <strong>{index + 1}</strong>
                &nbsp;
                <span 
                    className={titleClass.join(' ')}
                    style={styles.textId} 
                    onDoubleClick={()=>onDoubleClick(todo.id)}>
                    {todo.title}                    
                </span>
                &nbsp;
                &nbsp;
                &nbsp;
                &nbsp;
                Время создания: ({todo.date})
            </span>
            
            <button className='rm' onClick={()=> removeTodo(todo.id)}>
                &times;
            </button>
        </li>
    )
}

TodoItem.propTypes ={
    todo: PropTypes.object.isRequired,
    index: PropTypes.number.isRequired,
    onChange: PropTypes.func.isRequired,
    onDoubleClick: PropTypes.func.isRequired
}


export default TodoItem