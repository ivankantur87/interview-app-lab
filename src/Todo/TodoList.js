import React from "react";
import TodoItem from "./TodoItem";
import PropTypes  from "prop-types";

const styles = {
    ul: {
        listStyle: "none",
        margin: 0,
        padding: 0
    }
}

function TodoList(props) {
    return(
        <ul style={styles.ul}>
            { props.todos.map((todo, index)=>{
                return todo.corrected
                    ? (<form onSubmit={props.correctedTitle} key={todo.id} >
                        <input defaultValue={todo.title}/>
                        <button type='submit'>Изменить</button>
                    </form>)
                    : (<TodoItem 
                        todo={todo} 
                        key={todo.id} 
                        index={index} 
                        onChange={props.onToggle}
                        onDoubleClick={props.onDoubleC}/>)
            })}
        </ul>
    )
}

TodoList.propTypes = {
    todos: PropTypes.arrayOf(PropTypes.object).isRequired,
    onToggle: PropTypes.func.isRequired,
    onDoubleC: PropTypes.func.isRequired,
    correctedTitle: PropTypes.func.isRequired
}

export default TodoList